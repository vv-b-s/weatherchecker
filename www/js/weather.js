var OpenWeatherAppKey = "2c24436d9d9a44bc6d9eae99d7835bb9"; //5bb30b963a0d79993b96acd6ce552b0c

function getWeatherWithCityName() {
    var cityName = $('#city-name-input').val();
    let tempType = $('#select-btn').html();
    var weatherString = `http://api.openweathermap.org/data/2.5/weather?q=${cityName}&appid=${OpenWeatherAppKey}${tempType != 'C' ? '' : '&units=metric'}`;
    var forecastString = `http://api.openweathermap.org/data/2.5/forecast?q=${cityName}&appid=${OpenWeatherAppKey}${tempType != 'C' ? '' : '&units=metric'}`;

    sendWeatherRequest(weatherString, forecastString);

    return false;
}

function onGetLocationSuccess(position) {
    //Изтегляне на информация за локацията на устройството от обекта position
    $('#get-weather-btn').prop('disabled', false);
    let tempType = $('#select-btn').html().trim();

    var latitude = position.coords.latitude;
    var longitude = position.coords.longitude;
    var weatherString = 'http://api.openweathermap.org/data/2.5/weather?lat='
        + latitude + '&lon=' + longitude + '&appid=' + OpenWeatherAppKey + `${tempType != 'C' ? '' : '&units=metric'}`;

    var forecastString = 'http://api.openweathermap.org/data/2.5/forecast?lat='
        + latitude + '&lon=' + longitude + '&appid=' + OpenWeatherAppKey + `${tempType != 'C' ? '' : '&units=metric'}`;


    sendWeatherRequest(weatherString, forecastString);
}

function sendWeatherRequest(weather, forecast) {

    $.getJSON(weather, showWeatherData).fail(jqXHR => {
        $('#error-msg').show();
        $('#error-msg').text("Error retrieving data. " + jqXHR.statusText);
    });

    $.getJSON(forecast, results => {
        let city = results.city.name;
        results = results.list;
        showForecastData(city, results);
    }).fail(jqXHR => {
        $('#error-msg').show();
        $('#error-msg').text("Error retrieving data. " + jqXHR.statusText);
    });
}

function showWeatherData(results) {
    if (results.weather.length) {
        $('#today').empty();
        $('#error-msg').hide();
        displayDataFromResult($('#today'), results.name, results);

    } else {
        $('#weather-data').hide();
        $('#error-msg').show();
        $('#error-msg').text("Error retrieving data. ");
    }
}

function showForecastData(city, results) {
    if (results.length) {
        $('#forecast').empty();
        $('#error-msg').hide();
        results.forEach(r => displayDataFromResult($('#forecast'), city, r));


    } else {
        $('#error-msg').show();
        $('#error-msg').text("Error retrieving data. ");
    }
}

function displayDataFromResult(container, city, results) {
    let tempType = $('#select-btn').html();

    let title = `${city} ${new Date(results.dt * 1000)}`;
    let weather = results.weather[0].main;
    let icon = `http://openweathermap.org/img/w/${results.weather[0].icon}.png`;
    let temperature = `${results.main.temp} ${tempType}`;
    let wind = results.wind.speed;
    let windDeg = results.wind.deg;
    let humidity = results.main.humidity;
    let visibility = results.visibility;
    let minTemp = results.main.temp_min;
    let maxTemp = results.main.temp_max;
    let desctiption = results.weather[0].description;
    var sunriseDate = new Date(results.sys.sunrise * 1000).toLocaleTimeString();
    var sunsetDate = new Date(results.sys.sunset * 1000).toLocaleTimeString();

    document.getElementById(container.attr('id')).innerHTML += getForecastComponent(title, weather, temperature, icon, wind, windDeg, humidity, visibility, sunriseDate,
        sunsetDate, minTemp, maxTemp, desctiption);
}

function getWeatherWithGeoLocation() {
    //Метод getCurrentPosition вика Cordova Geolocation API
    navigator.geolocation.getCurrentPosition(onGetLocationSuccess, onGetLocationError,
        {enableHighAccuracy: true});
    $('#error-msg').show();
    $('#error-msg').text('Determining your current location ...');
    $('#get-weather-btn').prop('disabled', true);
}

function onGetLocationError(error) {
    $('#error-msg').text('Error getting location');
    $('#get-weather-btn').prop('disabled', false);
}

function computeDirection(degrees) {
    if (degrees > 11.25 && degrees < 33.75) {
        return "NNE";
    } else if (degrees > 33.75 && degrees < 56.25) {
        return "ENE";
    } else if (degrees > 56.25 && degrees < 78.75) {
        return "E";
    } else if (degrees > 78.75 && degrees < 101.25) {
        return "ESE";
    } else if (degrees > 101.25 && degrees < 123.75) {
        return "ESE";
    } else if (degrees > 123.75 && degrees < 146.25) {
        return "SE";
    } else if (degrees > 146.25 && degrees < 168.75) {
        return "SSE";
    } else if (degrees > 168.75 && degrees < 191.25) {
        return "S";
    } else if (degrees > 191.25 && degrees < 213.75) {
        return "SSW";
    } else if (degrees > 213.75 && degrees < 236.25) {
        return "SW";
    } else if (degrees > 236.25 && degrees < 258.75) {
        return "WSW";
    } else if (degrees > 258.75 && degrees < 281.25) {
        return "W";
    } else if (degrees > 281.25 && degrees < 303.75) {
        return "WNW";
    } else if (degrees > 303.75 && degrees < 326.25) {
        return "NW";
    } else if (degrees > 326.25 && degrees < 348.75) {
        return "NNW";
    } else {
        return "N";
    }
}