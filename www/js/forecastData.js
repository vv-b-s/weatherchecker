function getForecastComponent(title, weather, temperature, icon, wind, windDeg, humidity, visibility, sunrise, sunset, minTemp, maxTemp, description) {
    return `   
  <ul data-role='listview' data-inset='true' class='ui-listview ui-listview-inset ui-corner-all ui-shadow'>
            <li data-role="list-divider" class="ui-li-divider ui-bar-a light-blue ui-bar-inherit ui-first-child" role="heading">${title}</li>
                    <li class="ui-li-static ui-body-inherit"><span>${weather} <span>${temperature}</span> <img src='${icon}'/></span></li>
                    <li class="ui-li-static ui-body-inherit">Вятър: <div class="compass"><div class="direction"><p>${computeDirection(windDeg)}<span>${wind} възела</span></p></div><div class="arrow ${computeDirection(windDeg).toLowerCase()}"></div></div></li>
                    <li class="ui-li-static ui-body-inherit">Влажност: <span>${humidity}</span> %</li>
                    <li class="ui-li-static ui-body-inherit">Видимост: <span>${visibility}</span></li>
                    <li class="ui-li-static ui-body-inherit">Изгрев: <span >${sunrise}</span></li>
                    <li class="ui-li-static ui-body-inherit">Залез: <span>${sunset}</span></li>
                    <li class="ui-li-static ui-body-inherit">Минимална температура: <span>${minTemp}</span></li>
                    <li class="ui-li-static ui-body-inherit">Максимална темпаратура: <span>${maxTemp}</span></li>
                    <li class="ui-li-static ui-body-inherit">Описание: <span>${description}</span></li>
                </ul>`
}